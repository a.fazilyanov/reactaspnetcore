import React, { useState } from 'react';
import axios from 'axios';



function App() {

  const [forecastData, setForecastData] = useState([]);

  const onClick = async (e) => {

    e.preventDefault();
    
    let dataFromApi = await axios.get('https://localhost:5001/WeatherForecast');
    setForecastData(dataFromApi.data);
  };

  return (
    <div className="App">
      <button onClick={onClick}>Get</button>
      {
        forecastData.map(
          (item) => 
          (<p>{item.date} / {item.temperatureC} / {item.summary}</p>)
        )
      }
    </div>
  );
}

export default App;
